# metadataify
a simple tool written in python3 to extract metadata from pdf files.
## Requirements
* `chardet`
* `pdfminer.six`
## Instructions
1. Put `metadataify.py` in a folder with pdf files
2. Run `./metadataify.py` in the command line.